const form$$ = document.querySelector('form')

form$$.addEventListener('submit', (e) => {
    e.preventDefault();

    const name = form$$.querySelector('#name').value;
    const price = form$$.querySelector('#price').value;
    const description = form$$.querySelector('#description').value;
    const urlImg = form$$.querySelector('#urlImg').value;
    const rating = form$$.querySelector('#rating').value;

    const newProduct = {
        name,
        price,
        description,
        urlImg,
        rating
    }

    console.log(name);
});

//Function to put new product
function addProduct(newProduct) {
    fetch('http://localhost:3000/products', {
        method: 'POST',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: newProduct
    }).then((res) => res.json()).then(resData => {
        console.log(resData);
    });
}

//Function to edit the selected product
function editProduct(editProduct) {
    fetch('http://localhost:3000/products', {
        method: 'PUT',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body:  JSON.stringify(newProduct)
    }).then((res) => res.json()).then(resData => {
        console.log(resData);
    });
}