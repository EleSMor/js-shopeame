document.querySelector('#Home').addEventListener('click', goHome)
document.querySelector('#Prods').addEventListener('click', goProducts)
document.querySelector('#Management').addEventListener('click', goManagement)

function goHome() {
    window.open('index.html', '_self');
} function goProducts() {
    window.open('products.html', '_self');
} function goManagement(productId) {
    window.open('management.html', '_self');
}

//Creamos función para pasar el url de la API
const apiCall = async (url) => {
    const res = await fetch('http://localhost:3000/' + url);
    return res.json();
}

//Creamos contenedor para los elementos de la pantalla
if (window.location.href.includes("products")) {
    row$$ = document.querySelector('[data-function="products"]');
    row$$.classList.add('d-flex', 'flex-wrap', 'justify-content-start', 'align-items-baseline');

    //Utilizamos la función para los productos
    apiCall('products').then(products => {
        const $numProds = document.querySelector('[data-function="numProds"]');
        $numProds.innerHTML = `<p>(${products.length})</p>`;

        for (const product of products) {
            createProduct(product);
        };
    });


    const createProduct = product => {
        row$$ = document.querySelector('[data-function="products"]');
        row$$.classList.add(
            'd-flex',
            'align-items-around',
            'justify-content-between'
        );

        const divProd$$ = document.createElement('div');
        const imgProd$$ = document.createElement('img');
        const nameProd$$ = document.createElement('p');
        const priceProd$$ = document.createElement('p');
        const descProd$$ = document.createElement('p');
        const divFoot$$ = document.createElement('div');
        const divStars$$ = document.createElement('div');
        const ratingProd$$ = document.createElement('p');
        const editBtn$$ = document.createElement('button');

        divProd$$.classList.add(
            'col-3',
            'col-m-3',
            'col-lg-3',
            'col-xl-3',
            'd-flex',
            'flex-column',
            'align-items-around',
            'justify-content-between'
        );

        imgProd$$.classList.add('b-prod__img');
        imgProd$$.setAttribute('src', product.image);

        nameProd$$.classList.add('b-prod__name');
        nameProd$$.textContent = product.name;

        priceProd$$.classList.add('b-prod__price');
        priceProd$$.textContent = `$${product.price}`;

        descProd$$.classList.add('b-prod__desc');
        descProd$$.textContent = product.description;

        divFoot$$.classList.add(
            'd-flex',
            'justify-content-between',
            'align-items-baseline',
        );

        divStars$$.classList.add(
            'd-flex',
            'justify-content-between',
            'align-items-baseline',
        );

        ratingProd$$.classList.add('b-prod__rating');
        ratingProd$$.textContent = product.stars;

        for (i = 0; i < product.stars; i++) {
            const spanStar$$ = document.createElement('span');
            spanStar$$.classList.add(
                'fa',
                'fa-star',
                'b-prod__rating',
            );

            divStars$$.appendChild(spanStar$$);
        }
        // if (product.stars !== product.stars) {
        //     divStars$$.lastChild.classList.replace('fa-star','fa-star-half');
        // }
        divStars$$.appendChild(ratingProd$$);

        editBtn$$.classList.add('b-prod__btn');
        editBtn$$.textContent = 'Editar';
        // divProd$$.addEventListener('click', () => {
        //     // console.log(product.id)
        //     goManagement(product.id)
        // })


        divFoot$$.appendChild(divStars$$);
        divFoot$$.appendChild(editBtn$$);

        divProd$$.appendChild(imgProd$$);
        divProd$$.appendChild(nameProd$$);
        divProd$$.appendChild(priceProd$$);
        divProd$$.appendChild(descProd$$);
        divProd$$.appendChild(divFoot$$);
        row$$.appendChild(divProd$$);
    }
}

if (window.location.href.includes("management")) {
    const form$$ = document.querySelector('[data-function="manager-form"]')

    form$$.addEventListener('submit', (e) => {
        e.preventDefault();

        const name = form$$.querySelector('#name').value;
        const price = form$$.querySelector('#price').value;
        const description = form$$.querySelector('#description').value;
        const image = form$$.querySelector('#urlImg').value;
        const stars = form$$.querySelector('#rating').value;

        const newProduct = {
            name,
            price,
            description,
            image,
            stars
        }
        addProduct(newProduct);
        goProducts();
    });
}
//Function to put new product
const addProduct = (newProduct) => {
    fetch('http://localhost:3000/products', {
        method: 'POST',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(newProduct)
    }).then((res) => res.json()).then(resData => {
        console.log(resData);
    });
}

//Function to edit the selected product
function editProduct(prod) {
    fetch('http://localhost:3000/products', {
        method: 'PUT',
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(prod)
    }).then((res) => res.json()).then(resData => {
        console.log(resData);
    });
}


// function iniciar(){
//     const urlParams = new URLSearchParams(window.location.search);
//     const id = urlParams('id');
//     const name = urlParams('name')
//     console.log(id);
//     console.log(name);
// }

// iniciar();